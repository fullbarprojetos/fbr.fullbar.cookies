# Fullbar Cookies

# Instalação
Para usar o componente de cookies, basta copiar o componente dentro do projeto e chamar no arquivo App.vue
```javascript
import Cookie from '@/components/Cookie'

export default {
  components: {
    Cookie
  }
}
```